// /**
//
//  @Name：layuiAdmin 用户管理 管理员管理 角色管理
//  @Author：star1029
//  @Site：http://www.layui.com/admin/
//  @License：LPPL
//
//  */
//
//
// layui.define(['table', 'form'], function(exports){
//     var $ = layui.$
//         ,table = layui.table
//         ,form = layui.form;
//
//     //用户管理
//     table.render({
//         elem: '#LAY-product-manage'
//         ,url: '/product/list'
//         ,cols: [[
//             //表头
//             {type: 'checkbox', fixed: 'left'}
//             ,{field: 'id', title: 'ID', width:80, sort: true, fixed: 'left'}
//             ,{field: 'category', title: '分类', width:100}
//             ,{field: 'name', title: '商品名', width:100}
//             ,{field: 'price', title: '价格', width: 180}
//             ,{field: 'stock', title: '库存', width:180}
//             ,{title: '操作', width: 150, align:'center', fixed: 'right', toolbar: '#table-useradmin-webuser'}
//         ]]
//         ,page: true
//         ,limit: 30
//         ,height: 'full-220'
//         ,text: '对不起，加载出现异常！'
//     });
//
//     //监听工具条
//     table.on('toolbar(LAY-product-manage)', function(obj){
//         var data = obj.data;
//         if(obj.event === 'del'){
//             layer.confirm('真的删除行么', function(index){
//                 obj.del();
//                 layer.close(index);
//                 // alert(data.id)  data.id对应field中的id的值
//                 $.ajax({
//                     type:'get',
//                     url:'/user/delById',
//                     data:{"id":data.id},
//                     success:function (res) {
//                         if (res.data.affectRow === 1){//删除成功
//                             layer.msg('删除成功', {
//                                 icon: 1,
//                                 time: 2000 //2秒关闭（如果不配置，默认是3秒）
//                             }, function(){
//                                 //do something
//                             });
//                         }else {//删除失败
//
//                             layer.msg('删除失败', {
//                                 icon: 1,
//                                 time: 2000 //2秒关闭（如果不配置，默认是3秒）
//                             }, function(){
//                                 //do something
//                             });
//
//                         }
//                     }
//                 })
//
//             });
//         } else if(obj.event === 'edit'){
//             var tr = $(obj.tr);
//             editDetail(data);
//         }
//     });
//
//     /**
//      * 根据id修改用户信息
//      */
//     function editDetail(data) {
//         layer.open({
//             type: 2
//             ,title: '编辑用户'
//             ,content: '/user/userForm'
//             ,maxmin: true
//             ,area: ['500px', '450px']
//             ,btn: ['确定', '取消']
//             ,yes: function(index, layero){
//                 var iframeWindow = window['layui-layer-iframe'+ index]
//                     ,submitID = 'LAY-product-front-submit'
//                     ,submit = layero.find('iframe').contents().find('#'+ submitID);
//
//                 //监听提交
//                 iframeWindow.layui.form.on('submit('+ submitID +')', function(data){
//                     var info = data.field; //获取提交的字段
//
//                     console.log(info)
//                     var id = info.id;
//                     var username = info.username;
//                     var phone = info.phone;
//                     var email = info.email;
//
//                     //提交 Ajax 成功后，静态更新表格中的数据
//                     //$.ajax({});
//                     $.ajax({
//                         type:'post',
//                         url:'/user/updateUser',
//                         data: {
//                             "id":id,
//                             "username":username,
//                             "phoneNumber":phone,
//                             "email":email,
//                         },
//                         success:function (res) {
//                             if (res.data.affectRow > 0){//修改成功
//                                 layer.msg('修改成功', {
//                                     icon: 1,
//                                     time: 2000 //2秒关闭（如果不配置，默认是3秒）
//                                 }, function(){
//                                     //do something
//                                 });
//                             }else {//修改失败
//                                 layer.msg('修改失败', {
//                                     icon: 1,
//                                     time: 2000 //2秒关闭（如果不配置，默认是3秒）
//                                 }, function(){
//                                     //do something
//                                 });
//
//                             }
//                         }
//                     })
//
//                     table.reload('LAY-product-front-submit'); //数据刷新
//                     layer.close(index); //关闭弹层
//                 });
//                 submit.trigger('click');
//             }
//             ,success: function(layero, index){
//                 /**
//                  * 给layer弹出层的输入框初始化赋值！
//                  */
//                 var body = layer.getChildFrame('body', index);
//                 // console.log(data);
//                 body.find("#edit_id")[0].value=data.id;
//                 body.find("#edit_phone")[0].value=data.phoneNumber;
//                 body.find("#edit_email")[0].value=data.email;
//                 body.find("#edit_email")[0].value=data.email;
//                 body.find("#edit_pswd")[0].value=data.password;
//             }
//         });
//     }
//
//
//
//
//
//
//
//
//
//
//
//     //管理员管理
//     table.render({
//         elem: '#LAY-user-back-manage'
//         ,url: layui.setter.base + 'json/useradmin/mangadmin.js' //模拟接口
//         ,cols: [[
//             {type: 'checkbox', fixed: 'left'}
//             ,{field: 'id', width: 80, title: 'ID', sort: true}
//             ,{field: 'loginname', title: '登录名'}
//             ,{field: 'telphone', title: '手机'}
//             ,{field: 'email', title: '邮箱'}
//             ,{field: 'role', title: '角色'}
//             ,{field: 'jointime', title: '加入时间', sort: true}
//             ,{field: 'check', title:'审核状态', templet: '#buttonTpl', minWidth: 80, align: 'center'}
//             ,{title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#table-useradmin-admin'}
//         ]]
//         ,text: '对不起，加载出现异常！'
//     });
//
//     //监听工具条
//     table.on('tool(LAY-user-back-manage)', function(obj){
//         var data = obj.data;
//         if(obj.event === 'del'){
//             layer.prompt({
//                 formType: 1
//                 ,title: '敏感操作，请验证口令'
//             }, function(value, index){
//                 layer.close(index);
//                 layer.confirm('确定删除此管理员？', function(index){
//                     console.log(obj)
//                     obj.del();
//                     layer.close(index);
//                 });
//             });
//         }else if(obj.event === 'edit'){
//             var tr = $(obj.tr);
//
//             layer.open({
//                 type: 2
//                 ,title: '编辑管理员'
//                 ,content: '../../../views/user/administrators/adminform.html'
//                 ,area: ['420px', '420px']
//                 ,btn: ['确定', '取消']
//                 ,yes: function(index, layero){
//                     var iframeWindow = window['layui-layer-iframe'+ index]
//                         ,submitID = 'LAY-user-back-submit'
//                         ,submit = layero.find('iframe').contents().find('#'+ submitID);
//
//                     //监听提交
//                     iframeWindow.layui.form.on('submit('+ submitID +')', function(data){
//                         var field = data.field; //获取提交的字段
//
//                         //提交 Ajax 成功后，静态更新表格中的数据
//                         //$.ajax({});
//                         table.reload('LAY-product-front-submit'); //数据刷新
//                         layer.close(index); //关闭弹层
//                     });
//
//                     submit.trigger('click');
//                 }
//                 ,success: function(layero, index){
//
//                 }
//             })
//         }
//     });
//
//     //角色管理
//     table.render({
//         elem: '#LAY-user-back-role'
//         ,url: layui.setter.base + 'json/useradmin/role.js' //模拟接口
//         ,cols: [[
//             {type: 'checkbox', fixed: 'left'}
//             ,{field: 'id', width: 80, title: 'ID', sort: true}
//             ,{field: 'rolename', title: '角色名'}
//             ,{field: 'limits', title: '拥有权限'}
//             ,{field: 'descr', title: '具体描述'}
//             ,{title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#table-useradmin-admin'}
//         ]]
//         ,text: '对不起，加载出现异常！'
//     });
//
//     //监听工具条
//     table.on('tool(LAY-user-back-role)', function(obj){
//         var data = obj.data;
//         if(obj.event === 'del'){
//             layer.confirm('确定删除此角色？', function(index){
//                 obj.del();
//                 layer.close(index);
//             });
//         }else if(obj.event === 'edit'){
//             var tr = $(obj.tr);
//
//             layer.open({
//                 type: 2
//                 ,title: '编辑角色'
//                 ,content: '../../../views/user/administrators/roleform.html'
//                 ,area: ['500px', '480px']
//                 ,btn: ['确定', '取消']
//                 ,yes: function(index, layero){
//                     var iframeWindow = window['layui-layer-iframe'+ index]
//                         ,submit = layero.find('iframe').contents().find("#LAY-user-role-submit");
//
//                     //监听提交
//                     iframeWindow.layui.form.on('submit(LAY-user-role-submit)', function(data){
//                         var field = data.field; //获取提交的字段
//
//                         //提交 Ajax 成功后，静态更新表格中的数据
//                         //$.ajax({});
//                         table.reload('LAY-user-back-role'); //数据刷新
//                         layer.close(index); //关闭弹层
//                     });
//
//                     submit.trigger('click');
//                 }
//                 ,success: function(layero, index){
//
//                 }
//             })
//         }
//     });
//
//     exports('useradmin', {})
// });