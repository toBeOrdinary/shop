package com.suse.shop.controller;


import com.suse.shop.common.JsonResponse;
import com.suse.shop.entity.CartVO;
import com.suse.shop.entity.Product;
import com.suse.shop.entity.User;
import com.suse.shop.service.CartService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



@Controller
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    @RequestMapping("addToCart")
    public String addToCart(@RequestParam("proId")Integer proId, HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("currentUser");
        int affectRow = cartService.addToCart(proId,user.getId());
        System.out.println(user);
        Map<String,Integer> map = new HashMap<>();
        map.put("affectRow",affectRow);
        return "/views/user/objuser/myinfo";
    }

    /**
     * 根据id删除单个购物车商品
     */
    @RequestMapping("delById")
    @ResponseBody
    public JsonResponse deleteByIds(@RequestParam("id")Integer id){
        int affectRow = cartService.delById(id);
        Map<String,Integer> map = new HashMap<>();
        map.put("affectRow",affectRow);
        return JsonResponse.success(map);
    }
    /**
     * 根据id批量删除购物车商品
     */
    @RequestMapping("deleteByIds")
    @ResponseBody
    public JsonResponse deleteById(@RequestParam("ids[]")Integer[] ids){
        List<Integer> idList = Arrays.asList(ids);

        int affectRow = cartService.delCartByIds(idList);
        Map<String,Integer> map = new HashMap<>();
        map.put("affectRow",affectRow);
        return JsonResponse.success(map);
    }
    @RequestMapping("myCart")
    public String  toMyCart(){
        return "/views/user/objuser/mycart";
    }

    @GetMapping("myCartList")
    @ResponseBody
    public Map<String, Object>  myCartList(@RequestParam("userId")Integer user_id,
                                           @RequestParam("page") Integer id,
                                           @RequestParam("limit") Integer pageSize){
        List<CartVO> carts = cartService.getMyCartList(user_id);
        Map<String,Object> map = new HashMap<>();
        map.put("data",carts);
        map.put("code",0);
        map.put("msg","success");
        map.put("count",carts.size());
        return map;
    }
}
