package com.suse.shop.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    @RequestMapping("/")
    public String toIndex(){
        return "index";
    }
    @RequestMapping("shopDetail")
    public String shopDetail(){
        return "shop-detail";
    }
//
//    @RequestMapping("/manage")
//    public String toManage(){
//        return "manage";
//    }

    @RequestMapping("/login")
    public String toLogin(){
        return "login";
    }

}


