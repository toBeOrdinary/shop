package com.suse.shop.controller;

import com.suse.shop.common.JsonResponse;
import com.suse.shop.entity.Product;
import com.suse.shop.entity.User;
import com.suse.shop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @RequestMapping("testList")
    public ModelAndView testList(){
        List<Product> list = productService.listAll();
        ModelAndView modelAndView  =new ModelAndView();
        modelAndView.addObject("list" , list);
        modelAndView.setViewName("/index");
        return modelAndView;
    }

    @RequestMapping("productForm")
    public String toProductForm(){
        return "views/product/productform";
    }

    @RequestMapping("/list")
    @ResponseBody
    public Map<String, Object> list(){
        List<Product> products = productService.listAll();
        Map<String,Object> map = new HashMap<>();
        map.put("data",products);
        map.put("code",0);
        map.put("msg","success");
        map.put("count",products.size());
        return map;
    }

    /**
     * 根据ids批量删除
     */
    @PostMapping("delByIds")
    @ResponseBody
    public JsonResponse delByIds(@RequestParam("ids[]")Integer[] ids){
        List<Integer> idList = Arrays.asList(ids);
        int affectRow = productService.delByIds(idList);

        Map<String,Integer> map = new HashMap<>();
        map.put("affectRow",affectRow);
        return JsonResponse.success(map);
    }

    /**
     * 根据id删除单个商品
     */
    @GetMapping("delById")
    @ResponseBody
    public JsonResponse delById(@RequestParam("id")Integer id){
        int affectRow = productService.delById(id);
        Map<String,Integer> map = new HashMap<>();
        map.put("affectRow",affectRow);
        return JsonResponse.success(map);
    }

    /**
     * 根据id修改商品信息
     */
    @PostMapping("editById")
    @ResponseBody
    public JsonResponse editById(@RequestParam("id")int id,
                                 @RequestParam("category")String category,
                                 @RequestParam("name")String name,
                                 @RequestParam("price")double price,
                                 @RequestParam("stock")int stock){
        int affectRow = productService.editById(id,category,name,price,stock);
        Map<String,Integer> map = new HashMap<>();
        map.put("affectRow",affectRow);
        return JsonResponse.success(map);
    }


    /**
     * 添加商品
     */
    @PostMapping("add")
    @ResponseBody
    public JsonResponse editById(@RequestParam("category")String category,
                                 @RequestParam("name")String name,
                                 @RequestParam("price")double price,
                                 @RequestParam("stock")int stock){
        int affectRow = productService.add(category,name,price,stock);
        Map<String,Integer> map = new HashMap<>();
        map.put("affectRow",affectRow);
        return JsonResponse.success(map);
    }


}
