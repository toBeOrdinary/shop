package com.suse.shop.controller;


import com.suse.shop.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

//后台controller
@Controller

@RequestMapping("/manage")
public class ManageController {

    @RequestMapping("")
    public String toManage(HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("currentUser");
        if (user!=null && user.getUsername().equals("admin")){
            return "manage";
        }
        return "manage_dissmison";
    }

    @RequestMapping("/user/list")
    public String toUserManage(){
        return "views/user/user/list";
    }


    @RequestMapping("/product/list")
    public String toProductList(){
        return "views/product/list";
    }

    @RequestMapping("/product/order")
    public String toProductOrder(){
        return "views/product/order";
    }
    @RequestMapping("/product/category")
    public String toProductCategory(){
        return "views/product/category";
    }
    @RequestMapping("/cart")
    public String toCart(){
        return "views/product/cart";
    }
    @RequestMapping("/home/console")
    public String toConsole(){
        return "views/home/console";
    }

}
