package com.suse.shop.controller;


import com.suse.shop.common.JsonResponse;
import com.suse.shop.entity.Cart;
import com.suse.shop.entity.CartVO;
import com.suse.shop.entity.RegisterStatus;
import com.suse.shop.entity.User;
import com.suse.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;


@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 登录
     */
    @PostMapping("login")
    @ResponseBody
    public JsonResponse login(@RequestParam(value = "phoneOrEmail")String phoneOrEmail,
                              @RequestParam(value = "password")String password,
                              HttpServletRequest request){
        User user = userService.login(phoneOrEmail,password);

        HttpSession session = request.getSession();
        session.setAttribute("currentUser",user);
        System.out.println(user);
        return JsonResponse.success(user);
        }

    /**
     * 检查手机号/邮箱被注册
     */
    @GetMapping("getPhoneOrEmail")
    @ResponseBody
    public JsonResponse registerName(@RequestParam(value = "phoneOrEmail")String phoneOrEmail){
        User user = userService.getPhoneOrEmail(phoneOrEmail);
        return JsonResponse.success(user);
    }

    /**
     * 注册
     */
    @PostMapping("register")
    @ResponseBody
    public JsonResponse register(@RequestParam(value = "phoneOrEmail")String phoneOrEmail,
                                 @RequestParam(value = "password")String password){
        RegisterStatus status = userService.register(phoneOrEmail,password);
        return JsonResponse.success(status);
    }

    /**
     * 查询所有用户
     */
    @GetMapping("list")
    @ResponseBody
    public Map<String, Object> list(){
        List<User> users = userService.listAll();
        Map<String,Object> map = new HashMap<>();
        map.put("data",users);
        map.put("code",0);
        map.put("msg","success");
        map.put("count",users.size());
        return map;
    }

    /**
     * 根据id删除
     */
    @GetMapping("delById")
    @ResponseBody
    public JsonResponse delById(@RequestParam(value = "id")int id){
        Map<String,Integer> map = new HashMap<>();
        int affectRow = userService.delById(id);
        map.put("affectRow",affectRow);
        return JsonResponse.success(map);
    }
    /**
     * 根据ids批量删除
     */
    @PostMapping("delByIds")
    @ResponseBody
    public JsonResponse delByIds(@RequestParam("ids[]")Integer[] ids){
        List<Integer> idList = Arrays.asList(ids);
        int affectRow = userService.delByIds(idList);

        Map<String,Integer> map = new HashMap<>();
        map.put("affectRow",affectRow);
        return JsonResponse.success(map);
    }

    /**
     * --面向用户操作--
     * 用户信息编辑
     */
    @RequestMapping("myInfo")
    public String toMyInfo(){
        return "/views/user/objuser/myinfo";
    }

    /**
     * 用户个人信息页面--面向用户
     */
    @RequestMapping("myInfoOption")
    public String toMyInfoOption(){
        return "/views/user/objuser/my_info_option";
    }



    /**
     *用户个人信息数据接口---面向用户
     */
    @GetMapping("myInfoList")
    @ResponseBody
    public Map<String, Object> myInfoList(
                                    @RequestParam("userId")Integer id
                                    ,@RequestParam("page") Integer page
                                    , @RequestParam("limit") Integer pageSize){
        User user = userService.myInfo(id);
        List<User> users = new ArrayList<>();//必须传List<User>到map中，前端才显示数据 why?
        users.add(user);

        Map<String,Object> map = new HashMap<>();
        map.put("data",users);
        map.put("code",0);
        map.put("msg","success");
        map.put("count",user==null?0:1);
        return map;
    }

    /**
     * --面向管理员操作
     * 用户个人信息管理
     */
    @RequestMapping("userForm")
    public String toUserForm(){
        return "/views/user/user/userform";
    }

    /**
     * 面向管理员
     * 根据id修改用户
     */
    @PostMapping("updateUser")
    @ResponseBody
    public JsonResponse updateUserById(@RequestParam(value = "id")int id,
                                       @RequestParam(value = "username")String username,
                                       @RequestParam(value = "phoneNumber")String phoneNumber,
                                       @RequestParam(value = "email")String email){
        Map<String,Integer> map = new HashMap<>();
        int affectRow = userService.updateUserById(id,username,phoneNumber,email);
        map.put("affectRow",affectRow);
        return JsonResponse.success(map);
    }

    /**
     * 面向用户
     * 根据id修改用户
     */
    @PostMapping("editMyInfo")
    @ResponseBody
    public JsonResponse editMyInfo(@RequestParam(value = "id")Integer id,
                                       @RequestParam(value = "username")String username,
                                       @RequestParam(value = "phoneNumber")String phoneNumber,
                                       @RequestParam(value = "password")String password,
                                       @RequestParam(value = "email")String email){
        Map<String,Integer> map = new HashMap<>();
        int affectRow = userService.editMyInfo(id,username,phoneNumber,email,password);
        map.put("affectRow",affectRow);
        return JsonResponse.success(map);
    }
    /**
     * 插入新用户
     */
    @PostMapping("insertUser")
    @ResponseBody
    public JsonResponse insertUser(@RequestParam(value = "username")String username,
                                   @RequestParam(value = "phoneNumber")String phoneNumber,
                                   @RequestParam(value = "password")String password,
                                   @RequestParam(value = "email")String email){
        Map<String,Integer> map = new HashMap<>();
        System.out.println(username);
        System.out.println(phoneNumber);
        System.out.println(password);
        System.out.println(email);
        int affectRow = userService.insertUser(username,password,phoneNumber,email);
        map.put("affectRow",affectRow);
        return JsonResponse.success(map);
    }


    /**
     * 返回json格式数据一定要加@ResponseBody！！！！
     */
}
