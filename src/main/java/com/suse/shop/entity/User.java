package com.suse.shop.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;


/**
 * @TableField(*)  代表对应数据库表中“*”字段
 */
@Data
@TableName("user")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class User {
    private int id;
    private String username;
    private String password;
    @TableField("email")
    private String email;
    @TableField("phonenumber")
    private String phoneNumber;
    @TableField("lastlogin")
    private Date lastLogin;

}
