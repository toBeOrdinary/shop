package com.suse.shop.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@TableName("product")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Product {

    private int id;
    private String category;
    private String name;
    private double price;
    private int stock;


}
