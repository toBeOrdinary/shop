package com.suse.shop.entity;


import lombok.Data;

/**
 * 用户注册返回状态
 */
@Data
public class RegisterStatus {
    private int code;
}
