package com.suse.shop.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@TableName("cart")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Cart {
    private int id;
    private int proId;//产品编号
    private int userId;//用户id
    private int quantity;//数量

    private Product product;

}
