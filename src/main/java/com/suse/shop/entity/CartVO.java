package com.suse.shop.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CartVO {
    private int id;
    private int proId;//产品编号
    private String category;
    private String name;
    private double price;
    private int quantity;


}
