package com.suse.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suse.shop.entity.Cart;
import com.suse.shop.entity.CartVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface CartMapper extends BaseMapper<Cart> {
    @Select("SELECT cart.id,cart.pro_id,product.category,product.name,product.price,cart.quantity FROM cart,product where cart.pro_id=product.id and user_id=#{userId}")
    List<CartVO> getCart(@Param("userId") Integer id);
}
