package com.suse.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suse.shop.entity.Product;
import org.apache.ibatis.annotations.Update;

public interface ProductMapper extends BaseMapper<Product> {

    @Update("update product set stock=stock-1 where id=#{proId}")
    int updateStock(Integer proId);
}
