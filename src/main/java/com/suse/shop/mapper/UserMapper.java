package com.suse.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suse.shop.entity.Cart;
import com.suse.shop.entity.CartVO;
import com.suse.shop.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface UserMapper extends BaseMapper<User> {

}
