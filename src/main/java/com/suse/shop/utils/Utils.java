package com.suse.shop.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public boolean isPhoneNumber(String phonenNumber) {
        String PHONE_NUMBER_REG = "^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
        Pattern p = Pattern.compile(PHONE_NUMBER_REG);
        Matcher m = p.matcher(phonenNumber);
        return m.matches();
    }
    public boolean isEmail(String email){
        if ((email != null) && (!email.isEmpty())) {
            return Pattern.matches("^(\\w+([-.][A-Za-z0-9]+)*){3,18}@\\w+([-.][A-Za-z0-9]+)*\\.\\w+([-.][A-Za-z0-9]+)*$", email);
        }
        return false;
    }
}
