package com.suse.shop.service;

import com.suse.shop.entity.Product;

import java.util.List;

public interface ProductService {

     List<Product> listAll();

    int delByIds(List<Integer> idList);

    int delById(Integer id);

    int editById(int id, String category, String name, double price, int stock);

    int add(String category, String name, double price, int stock);
}
