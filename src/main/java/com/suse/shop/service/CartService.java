package com.suse.shop.service;

import com.suse.shop.entity.CartVO;

import java.util.List;

public interface CartService {


    List<CartVO> getMyCartList(Integer id);

    int delCartByIds(List<Integer> idList);


    int delById(Integer id);

    int addToCart(Integer id,Integer userId);
}
