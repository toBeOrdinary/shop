package com.suse.shop.service.impl;

import com.suse.shop.entity.Cart;
import com.suse.shop.entity.CartVO;
import com.suse.shop.mapper.CartMapper;
import com.suse.shop.mapper.ProductMapper;
import com.suse.shop.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartServiceImpl implements CartService {
    @Autowired
    CartMapper cartMapper;

    @Autowired
    ProductMapper productMapper;

    @Override
    public List<CartVO> getMyCartList(Integer id) {
        return cartMapper.getCart(id);
    }

    @Override
    public int delCartByIds(List<Integer> idList) {
        return cartMapper.deleteBatchIds(idList);
    }

    /**
     * 根据id删除购物车商品
     */
    @Override
    public int delById(Integer id) {
        return cartMapper.deleteById(id);
    }

    @Override
    public int addToCart(Integer proId,Integer userId) {
        int affectRow = productMapper.updateStock(proId);
        if (affectRow==0)
            return 0;
        return cartMapper.insert(new Cart().setProId(proId).setUserId(userId).setQuantity(1));
    }
}
