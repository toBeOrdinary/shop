package com.suse.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.suse.shop.entity.Product;
import com.suse.shop.mapper.ProductMapper;
import com.suse.shop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductMapper productMapper;


    @Override
    public List<Product> listAll() {
        QueryWrapper<Product> wrapper = new QueryWrapper<>();
        List<Product> products = productMapper.selectList(wrapper);
        return products.size()==0?null:products;
    }

    @Override
    public int delByIds(List<Integer> ids) {
        return productMapper.deleteBatchIds(ids);
    }

    @Override
    public int delById(Integer id) {
        return productMapper.deleteById(id);
    }

    @Override
    public int editById(int id, String category, String name, double price, int stock) {
        Product product = new Product().setId(id).setCategory(category).setName(name).setPrice(price).setStock(stock);
        return productMapper.updateById(product);
    }

    @Override
    public int add(String category, String name, double price, int stock) {
        Product product = new Product().setCategory(category).setName(name).setPrice(price).setStock(stock);
        return productMapper.insert(product);
    }
}
