package com.suse.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suse.shop.entity.Cart;
import com.suse.shop.entity.CartVO;
import com.suse.shop.entity.RegisterStatus;
import com.suse.shop.entity.User;
import com.suse.shop.mapper.UserMapper;
import com.suse.shop.service.UserService;
import com.suse.shop.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.rmi.CORBA.Util;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>  implements UserService {

    @Autowired
    private UserMapper userMapper;


    /**
     * @author 郑旭东
     * 登录功能
     * @return 用户实体 | null
     * 手机号/邮箱格式不正确 直接返回null
     */
    @Override
    public User login(String phone_email, String password) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();

        Utils utils = new Utils();
        //判断传过来的是手机号还是邮箱
        if (utils.isPhoneNumber(phone_email)){//手机号判断
            wrapper.eq("phonenumber",phone_email).eq("password",password);
        }else if(utils.isEmail(phone_email)){//邮箱判断
            wrapper.eq("email",phone_email).eq("password",password);
        }
        else{//如果既不是手机号又不是邮箱，应该直接return null，这里方便登录测试，不合法的手机号/邮箱格式也能登录
            wrapper.eq("phonenumber",phone_email).or().eq("email",phone_email).eq("password",password);
        }
        return userMapper.selectOne(wrapper);
    }

    //查看手机号或邮箱是否已被注册
    @Override
    public User getPhoneOrEmail(String phoneOrEmail) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();

        List<User> users;

        Utils utils = new Utils();
        //判断传过来的是手机号还是邮箱
        if (utils.isPhoneNumber(phoneOrEmail)){//手机号判断
            wrapper.eq("phonenumber",phoneOrEmail);
            users = userMapper.selectList(wrapper);
        }else if(utils.isEmail(phoneOrEmail)){//邮箱判断
            wrapper.eq("email",phoneOrEmail);
            users = userMapper.selectList(wrapper);
        }else {//都不是直接返回null
            return null;
        }

        //new了一个新的User对象只对其设置用户名
        return users.size()>0?users.get(0).setPassword(""): null;
    }

    //注册
    @Override
    public RegisterStatus register(String phoneOrEmail, String password) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();

        RegisterStatus status = new RegisterStatus();
        Utils utils = new Utils();
        if (utils.isPhoneNumber(phoneOrEmail)){//手机号判断
            status.setCode(userMapper.insert(new User()
                                                    .setPhoneNumber(phoneOrEmail)
                                                    .setPassword(password)));
        }else if(utils.isEmail(phoneOrEmail)){//邮箱判断
            status.setCode(userMapper.insert(new User().setEmail(phoneOrEmail).setPassword(password)));
        }
        return status;
    }

    /**
     * 返回所有用户
     */
    @Override
    public List<User> listAll() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        List<User> users = userMapper.selectList(wrapper);
        return users.size()==0?null:users;
    }

    /**
     * 根据id删除
     */
    @Override
    public int delById(int id) {
        int affectRow = userMapper.deleteById(id);
        return affectRow;
    }

    /**
     * 根据id批量删除用户
     */
    @Override
    public int delByIds(List<Integer> ids) {
        int affectRow = userMapper.deleteBatchIds(ids);
        return affectRow;
    }

    /**
     * 根据id更新用户
     */
    @Override
    public int updateUserById(int id,String username,String phoneNumber,String email) {
        User user = new User();
        user.setId(id);
        user.setUsername(username);
        user.setPhoneNumber(phoneNumber);
        user.setEmail(email);
        return userMapper.updateById(user);
    }

    /**
     * 插入新用户
     */
    @Override
    public int insertUser(String username,String password, String phoneNumber, String email) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setPhoneNumber(phoneNumber);
        user.setEmail(email);
        return userMapper.insert(user);
    }

    /**
     * 面向用户
     * 返回用户的个人信息
     */
    @Override
    public User myInfo(int id) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("id",id);
        return userMapper.selectOne(wrapper);
    }

    /**
     * 面向用户
     * 用户个人信息修改
     */
    @Override
    public int editMyInfo(Integer id, String username, String phoneNumber, String email, String password) {
        User user = new User().setId(id).setUsername(username).setPassword(password).setEmail(email).setPhoneNumber(phoneNumber);
        return userMapper.updateById(user);

    }



}
