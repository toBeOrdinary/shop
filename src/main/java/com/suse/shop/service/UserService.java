package com.suse.shop.service;

import com.suse.shop.common.JsonResponse;
import com.suse.shop.entity.Cart;
import com.suse.shop.entity.CartVO;
import com.suse.shop.entity.RegisterStatus;
import com.suse.shop.entity.User;

import java.util.List;


public interface UserService {


    User login(String username, String password);

    User getPhoneOrEmail(String phoneOrEmail);

    RegisterStatus register(String phoneOrEmail, String password);

    List<User> listAll();

    int delById(int id);

    int delByIds(List<Integer> ids);

    int updateUserById(int id,String username,String phoneNumber,String email);

    int insertUser(String username,String password, String phoneNumber, String email);

    User myInfo(int id);

    int editMyInfo(Integer id, String username, String phoneNumber, String email, String password);


}
